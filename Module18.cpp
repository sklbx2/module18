#include <iostream>
#include <conio.h>

const int g_name_size = 5;

void enterName(char name[])
{
    int i = 0;
    for (; i < g_name_size; i++)
    {
        for (int j = g_name_size - i; j > 0; j--)
        {
            std::cout << '_';
        }
        for (int j = g_name_size - i; j > 0; j--)
        {
            std::cout << '\b';
        }

        char ch = _getch();
        std::cout << ch;
        if (ch == '\r')
        {  
            break;
        }
        name[i] = ch;
    }
    name[i] = '\0';
    std::cout << '\n';
}

class Player
{
    char m_name[g_name_size+1];
    int m_score = 0;

public:
    Player(char name[], int score) : m_score{score}
    {
        strcpy_s(m_name, name);
    }

    const char* getName() { return m_name; }
    const int getScore() { return m_score; }

    friend bool operator> (const Player& p1, const Player& p2);
};

bool operator> (const Player& p1, const Player& p2)
{
    return p1.m_score > p2.m_score;
}

void selectionSort(Player* array[], int size)
{
    int max_index;
    // ���� ������ ������� �������
    for (int i = 0; i < size; i++)
    {
        max_index = i;
        // ���� ���������� � ������� ����� ����������
        for (int k = i + 1; k < size; k++)
        {
            // ���������� ���������
            if (*array[k] > *array[max_index])
                max_index = k;
        }
        // ������������ �������
        if (array[i] != array[max_index])
        {
            auto temp = array[i];
            array[i] = array[max_index];
            array[max_index] = temp;
        }
    }
    return;
}

int main()
{
    std::cout << "How many players do you want?\n";
    int players_num;
    std::cin >> players_num;
    std::cin.ignore(32767, '\n');   // ������� ������ ����� ������ �� ������ �����

    auto PlayerArray = new Player*[players_num];

    char name[g_name_size+1];
    int score;
    for (int i = 0; i < players_num; i++)
    {
        std::cout << "Enter player " << i + 1 << " name ("<< g_name_size <<" symbols max):\n";
        enterName(name);
        std::cout << "Enter player " << i + 1 << " score:\n";
        std::cin >> score;
        std::cin.ignore(32767, '\n');

        PlayerArray[i] = new Player(name, score);
    }

    selectionSort(PlayerArray, players_num);

    std::cout << "\nLeaderboard:\nPlace\tName\tScore\n";
    for (int i = 0; i < players_num; i++)
    {
        std::cout << i + 1 << '\t' << PlayerArray[i]->getName() << '\t' << PlayerArray[i]->getScore() << '\n';
    }


    for (int i = 0; i < players_num; i++)
    {
       delete PlayerArray[i];
    }
    delete[] PlayerArray;

    return 0;
}

